import jwt from 'jsonwebtoken';
import { JWT_SECRET, JWT_TOKEN_EXPIRES_IN } from '../config';

export default {
  genToken(data) {
    return jwt.sign(data, JWT_SECRET, {expiresIn: JWT_TOKEN_EXPIRES_IN });
  },

  verify(token) {
    return jwt.verify(token, JWT_SECRET);
  },
};
