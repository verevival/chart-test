import Router from 'koa-router';
import auth from './auth';
import chart from './chart';

const router = new Router({prefix: '/api'});

router.use(auth);
router.use(chart);

export default router.routes();
