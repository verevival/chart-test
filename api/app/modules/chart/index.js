import Router from 'koa-router';
import checkUser from '../../handlers/checkUser';
import { Chart } from './models';

const router = new Router();

router.get('/chart', checkUser(), async (ctx) => {
  const values = [];

  const chartValues = await Chart.find({});
  await chartValues.forEach( (item) => {
    values.push(item.value);
  });

  ctx.body = {data: values};
});

export default router.routes();
