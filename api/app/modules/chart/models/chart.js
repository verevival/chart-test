import mongose, { Schema } from 'mongoose';

const ChartSchema = new Schema({
  value: {
    type: Number,
  },
}, {
  timestamps: true,
});

ChartSchema.statics.createFields = ['value'];

export default mongose.model('chart', ChartSchema);

