import Koa from 'koa';
import connectorsInit from './connectors';
import initHandlers from './handlers';
import modules from './modules';
import jwtService from './services/jwt-service';
import {User} from './modules/users/models';
import {Chart} from './modules/chart/models';

const IO = require('koa-socket-2');
// const http = require('http');

connectorsInit();

const app = new Koa();
const io = new IO();

initHandlers(app);

app.use(modules);
app.use(async (ctx) => {
  ctx.body = '<h1>Api server</h1>';
});

io.attach(app);
io.on('connection', function(socket) {
  console.log('connected');
  socket.on('message', async (data) => {
    try {
      const { email } = await jwtService.verify(data.jwtoken);
      const user = await User.findOne({ email });

      if (!user) {
        socket.emit('on-error', 'User deleted');
      } else {
        const values = [];
        const chartValues = await Chart.find({});
        await chartValues.forEach( (item) => {
          values.push(item.value);
        });
        socket.emit( 'chart-data', {data: values});
      }
    } catch (e) {
      socket.emit('on-error', 'Unauthorized. Invalid Token');
    }

    /*
      Были данные на отдельном сервере
      const options = {
        port: 4000,
        hostname: 'localhost',
        path: '/api/chart',
        headers: {
          Authorization: data.jwtoken,
        },
      };

      http.get(options, function(res) {
        let body = '';

        res.on('data', function(data) {
          body += data;
        });

        res.on('end', function(e) {
          console.log(e);
          socket.emit( 'chart-data', JSON.parse(body));
        });
      });

    } else {
      try {
        socket.emit('on-error', 'No auth token');
      } catch (e) {
        console.log(e);
      }
    }
    */
  });

  socket.on('disconnect', () => {
    console.log('disconnect');
  });
});

export default app;
