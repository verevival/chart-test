import mongoose from 'mongoose';

mongoose.Promise = Promise;

export default (mongoUri) => {
  if (! mongoUri) {
    throw Error('Mongo uri is undefined');
  }

  return mongoose
      .connect(mongoUri,
          {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
          },
      )
      .then((mongoDb) => {
        console.log('MongoBD has been connected');
        return mongoDb;
      })
      .catch((e) => console.log(e));
};
