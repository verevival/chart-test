import { MONGO_URI } from '../config';
import mongooseConnector from '../connectors/mongoose-connectors';
import chartSeeds from './chart-seeds';

initSeeds().then(()=>{
  console.log('Seed data generated');
});

async function initSeeds() {
  const mongoConnection = await mongooseConnector(MONGO_URI);
  try {
    await chartSeeds();
  } catch (e) {
    console.log(e);
  }

  mongoConnection.disconnect();
}
