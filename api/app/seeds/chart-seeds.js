import _ from 'lodash';
import { Chart } from '../modules/chart/models';

function init() {
  const promises = [];

  _.times(5, (i) => {
    const userPromise = Chart.create({
      value: _.random(0, 100),
    });

    promises.push(userPromise);
  });

  return Promise.all(promises);
}

export default init;
