import React, {Component} from 'react';
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts';
import AuthForm from "./AuthForm";
import RegForm from "./RegForm";

class Chart extends Component {

    constructor(props) {
        super(props);

        this.state = {
            chartOptions: {
                xAxis: {
                    categories: ['A', 'B', 'C'],
                },
                series: [
                    { data: [1, 2, 3] }
                ]
            },
            socketOn: false,
            isAuth: false,
            token: '',
            showAuthForm: false,
            showRegForm: false,
        };
    }

    updateSeries = () => {

        const { socket } = this.props
        const { isAuth, token, socketOn } = this.state

        if (! socketOn) {
            this.setState( {socketOn: true});
            this.initSocketOn()
        }

        if (isAuth) {
            socket.emit('message', { jwtoken: token });
        } else {
            this.setState( {showAuthForm: true});
        }
    }

    showAuthForm = () => {
        const { showAuthForm } = this.state
        this.setState( {
            showAuthForm: true,
            showRegForm: false
        });

    }

    showRegForm = () => {
        const { showRegForm } = this.state
        this.setState( {
            showAuthForm: false,
            showRegForm: true
        });
    }


    setAuthData = (token) => {
        if (token) {
            this.setState( {
                token: token,
                isAuth: true
            });
            this.setState( {
                showAuthForm: false,
                showRegForm: false
            });
        }
    }

    logout = () => {
        this.setState( {
            token: '',
            isAuth: false
        });
    }

    initSocketOn = () => {
        const { socket } = this.props
        const { socketOn } = this.state

        if (socketOn) return

        socket.on('chart-data', (data) => {
            console.info('chart-data->',data)
            this.setState({
                chartOptions: {
                    series: [
                        { data: data.data}
                    ]
                }
            });
        })

        socket.on('on-error', (data) => {
            console.info('error-chart-data->',data)
        })
    }


    render() {
        const { chartOptions } = this.state;

        return (
            <div>
                {!this.state.isAuth && <button onClick={this.showAuthForm.bind(this)}>Login</button>}
                {!this.state.isAuth && <button onClick={this.showRegForm.bind(this)}>Registration</button> }
                {this.state.isAuth && <button onClick={this.logout.bind(this)}>Logout</button>}
                {this.state.showAuthForm && <AuthForm setAuthData={this.setAuthData}/>}
                {this.state.showRegForm && <RegForm parentState={this.setState} />}
                <HighchartsReact
                    highcharts={Highcharts}
                    options={chartOptions}
                />
                <button onClick={this.updateSeries.bind(this)}>Запросить данные по Socket.IO</button>
            </div>
        )
    }
}

export default Chart