import React, {Component} from "react";
import axios from "axios";

export default class AuthForm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            login: '',
            pass: ''
        }

    }

    submitHandler = event => {
        event.preventDefault()
    }

    changeInputHandler = event => {
        event.persist()
        this.setState(prev => ({...prev, ...{
            [event.target.name]: event.target.value
        }}))
    }

    login = async () => {
        console.log('login->', this)
        try {
            const response = await axios.post('http://localhost:4000/api/auth/sign-in', {
                email: this.state.login,
                password: this.state.pass,
            });

            this.props.setAuthData(response.data.data)
        } catch (e) {
            console.log(e);
        }
    };

    render() {
        return (
            <form onSubmit={this.submitHandler}>
                login: <input name="login" onChange={this.changeInputHandler} value={this.state.login} type="text"/>
                pass: <input name="pass" onChange={this.changeInputHandler} value={this.state.pass} type="password"/>
                <button onClick={this.login.bind(this)}>Войти</button>
            </form>
        )
    }
}