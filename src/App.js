import React from "react";
import Chart from "./Chart";
import socketIOClient from "socket.io-client";

const ENDPOINT = "http://localhost:4000";

function App() {

  const socket = socketIOClient(ENDPOINT);

  return (
    <div className="App">
      <Chart socket={socket}/>
    </div>
  );
}

export default App;
