import React, {Component} from "react";
import axios from "axios";

export default class RegForm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            login: '',
            name: '',
            pass: ''
        }
        console.log(this);
    }

    submitHandler = event => {
        event.preventDefault()
        this.registration().then((response) => {
            if (response) {
              alert('Пользователь ' + response.data.data.email + ' успешно зарегестрирован')
            }
            console.log(response);
        })
    }

    changeInputHandler = event => {
        event.persist()
        this.setState(prev => ({...prev, ...{
            [event.target.name]: event.target.value
        }}))
    }

    registration = async() => {
        const {login, name, pass} = this.state
        try {
            const response = await axios.post('http://localhost:4000/api/auth/sign-up', {
                email: login,
                name,
                password: pass,

            });
            return response
        } catch (e) {
            alert('Ошибка регистрации')
            console.log(e);
        }

        return false
    };

    render() {
        return (
            <form onSubmit={this.submitHandler}>
                login: <input name="login" onChange={this.changeInputHandler} value={this.state.login} type="text"/>
                name: <input name="name" onChange={this.changeInputHandler} value={this.state.name} type="text"/>
                pass: <input name="pass" onChange={this.changeInputHandler} value={this.state.pass} type="password"/>
                <button type="submit">Регистрация</button>
            </form>
        )
    }
}